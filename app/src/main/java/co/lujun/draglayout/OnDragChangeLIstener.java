package co.lujun.draglayout;

import android.view.View;

/**
 * Created by lujun on 15-11-27.
 */
public interface OnDragChangeLIstener {

    void onDragReleased(View releasedChild, float xvel, float yvel);
    void onDragPositionHorizontal(View child, int left, int dx);
    void onDragPositionVertical(View child, int top, int dy);
    void onDragStateChanged(int state);
    void onDragPositionChanged(View changedView, int left, int top, int dx, int dy);
    void onDragTryCaptureView(View child, int pointerId);
}
