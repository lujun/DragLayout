package co.lujun.draglayout;

import android.content.Context;
import android.graphics.Point;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by lujun on 2015/11/27.
 */
public class DragLinearLayout extends LinearLayout {

    private ViewDragHelper mViewDragHelper;
    private DragHelperCallback mCallback;
    private int mOriginLeft, mOriginTop;
    private float mSen = 1.0f; // 拖动敏感系数

    private static final String TAG = "DragLinearLayout";

    public DragLinearLayout(Context context){
        this(context, null);
    }

    public DragLinearLayout(Context context, AttributeSet attrs){
        this(context, attrs, 0);
    }

    public DragLinearLayout(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
        mCallback = new DragHelperCallback(this);
        mViewDragHelper = ViewDragHelper.create(this, mSen, mCallback);
        mCallback.setDragHelper(mViewDragHelper);
        mCallback.setReleaseListener(new OnDragChangeLIstener() {
            @Override
            public void onDragReleased(View releasedChild, float xvel, float yvel) {
                onFlip(releasedChild, xvel, yvel);
                Log.d(TAG, "onDragReleased--xvel:" + xvel + ", yvel:" + yvel);
            }

            @Override
            public void onDragPositionHorizontal(View child, int left, int dx) {
                Log.d(TAG, "onDragPositionHorizontal--left:" + left + ", dx:" + dx);
            }

            @Override
            public void onDragPositionVertical(View child, int top, int dy) {
                Log.d(TAG, "onDragPositionVertical--top:" + top + ", dy:" + dy);
            }

            @Override
            public void onDragStateChanged(int state) {
                Log.d(TAG, "onDragStateChanged--state:" + state);
            }

            @Override
            public void onDragPositionChanged(View changedView, int left, int top, int dx, int dy) {
                Log.d(TAG, "onDragPositionChanged--left:" + left + ", top:" + top + ", dx:" + dx + ", dy:" + dy);
            }

            @Override
            public void onDragTryCaptureView(View child, int pointerId) {
                mOriginLeft = child.getLeft();
                mOriginTop = child.getTop();
                Log.d(TAG, "onDragTryCaptureView--child.left:" + child.getLeft() + ", child.top:" + child.getTop());
            }
        });
    }

    @Override public boolean onInterceptHoverEvent(MotionEvent event) {
        int action = MotionEventCompat.getActionMasked(event);
        if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP){
            mViewDragHelper.cancel();
            return false;
        }
        return mViewDragHelper.shouldInterceptTouchEvent(event);
    }

    @Override public boolean onTouchEvent(MotionEvent event) {
        mViewDragHelper.processTouchEvent(event);
        return true;
    }

    @Override public void computeScroll() {
        super.computeScroll();
        if (mViewDragHelper.continueSettling(true)){// 动画进行中
            invalidate();
        }
    }

    @Override protected void onFinishInflate() {
        super.onFinishInflate();
        // 获取child view， getChildAt()
    }

    @Override protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        // get child view info

    }

    private void onFlip(View child, float xvel, float yvel){
        int finalLeft, finalTop;
        /*if (xvel < 0){// left
            finalLeft = -child.getWidth();
            finalTop = (int)child.getY();
        }else if (xvel > 0){ // right
            finalLeft = this.getWidth();
            finalTop = (int)child.getY();
        }*/
        if(xvel == 0 || yvel == 0) { // origin
            finalLeft = mOriginLeft;
            finalTop = mOriginTop;
        }else {
            Point point = onCalPosition(child, xvel, yvel);
            finalLeft = point.x;
            finalTop = point.y;
        }
        if(mViewDragHelper.smoothSlideViewTo(child, finalLeft, finalTop)){
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    /**
     * 计算release后的终点位置
     * @param child
     * @param xvel
     * @param yvel
     * @return
     */
    private Point onCalPosition(View child, float xvel, float yvel){
        float xDis = xvel < 0 ? child.getLeft() + child.getWidth()
                : this.getWidth() - child.getLeft();
        float yDis = yvel < 0 ? child.getTop() + child.getHeight()
                : this.getHeight() - child.getTop();
        float time = Math.max(xDis / Math.abs(xvel), yDis / Math.abs(yvel));
        return new Point((int)(xvel * time), (int)(yvel * time));
    }

    /**
     * set drag edge position
     * @param edge ViewDragHelper.EDGE_XXX
     */
    public void setDragEdge(int edge){
        if (mViewDragHelper == null){
            return;
        }
        mViewDragHelper.setEdgeTrackingEnabled(edge);
    }
}
