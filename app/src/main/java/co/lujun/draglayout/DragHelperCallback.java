package co.lujun.draglayout;

import android.support.v4.widget.ViewDragHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * Created by lujun on 2015/11/27.
 */
public class DragHelperCallback extends ViewDragHelper.Callback {

    private ViewGroup mViewGroup;
    private ViewDragHelper mDragHelper;
    private OnDragChangeLIstener mDragListener;
    private boolean ignoreEdge = true;

    public DragHelperCallback(ViewGroup viewGroup){
        this.mViewGroup = viewGroup;
    }

    public void setDragHelper(ViewDragHelper helper){
        this.mDragHelper = helper;
    }

    @Override public boolean tryCaptureView(View child, int pointerId) {
        if (mDragListener != null){
            mDragListener.onDragTryCaptureView(child, pointerId);
        }
        return true;
    }

    // 手指释放的时候回调
    @Override public void onViewReleased(View releasedChild, float xvel, float yvel) {
        super.onViewReleased(releasedChild, xvel, yvel);
        if (mDragListener != null){
            mDragListener.onDragReleased(releasedChild, xvel, yvel);
        }
    }

    @Override public int clampViewPositionHorizontal(View child, int left, int dx) {
        if (!instanceView()){
            throw new RuntimeException("ViewDragHelper must be in ViewGroup!");
        }
        if (mDragListener != null){
            mDragListener.onDragPositionHorizontal(child, left, dx);
        }
        // 边缘判断
        if (ignoreEdge){
            return left;
        }
        int leftX = mViewGroup.getPaddingLeft();
        int rightX = mViewGroup.getWidth() - child.getWidth();
        int newX = Math.min(Math.max(left, leftX), rightX);
        return newX;
    }

    @Override public int clampViewPositionVertical(View child, int top, int dy) {
        if (!instanceView()){
            throw new RuntimeException("ViewDragHelper must be in ViewGroup!");
        }
        if (mDragListener != null){
            mDragListener.onDragPositionVertical(child, top, dy);
        }
        // 边缘判断
        if (ignoreEdge){
            return top;
        }
        int topY = mViewGroup.getPaddingTop();
        int bottomY = mViewGroup.getHeight() - child.getHeight();
        int newY = Math.min(Math.max(top, topY), bottomY);
        return newY;
    }

    // 边缘滑动
    @Override public void onEdgeTouched(int edgeFlags, int pointerId) {
        super.onEdgeTouched(edgeFlags, pointerId);
    }

    @Override public void onEdgeDragStarted(int edgeFlags, int pointerId) {
        super.onEdgeDragStarted(edgeFlags, pointerId);
    }

    @Override public int getViewHorizontalDragRange(View child) {
        return mViewGroup.getMeasuredWidth() - child.getMeasuredWidth();
    }

    @Override public int getViewVerticalDragRange(View child) {
        return mViewGroup.getMeasuredHeight() - child.getMeasuredHeight();
    }

    // 当ViewDragHelper状态发生变化时回调（IDLE,DRAGGING,SETTING[自动滚动时]）
    @Override public void onViewDragStateChanged(int state) {
        super.onViewDragStateChanged(state);
        if (mDragListener != null){
            mDragListener.onDragStateChanged(state);
        }
    }

    // 当view位置发生改变
    @Override public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {
        super.onViewPositionChanged(changedView, left, top, dx, dy);
        if (mDragListener != null){
            mDragListener.onDragPositionChanged(changedView, left, top, dx, dy);
        }
    }

    // 当view被捕获
    /*@Override public void onViewCaptured(View capturedChild, int activePointerId) {
        super.onViewCaptured(capturedChild, activePointerId);
    }*/

    // true的时候会锁住当前的边界，false则unLock
    /*@Override public boolean onEdgeLock(int edgeFlags) {
        return super.onEdgeLock(edgeFlags);
    }*/

    // 改变同一个坐标（x,y）去寻找captureView位置的方法
    /*@Override public int getOrderedChildIndex(int index) {
        return super.getOrderedChildIndex(index);
    }*/

    private boolean instanceView(){
        if (mViewGroup instanceof LinearLayout
                || mViewGroup instanceof RelativeLayout
                || mViewGroup instanceof FrameLayout){
            return true;
        }
        return false;
    }

    /**
     * Is ignore edge when drag child view to edge
     * @param ignoreEdge
     */
    public void setIgnoreEdge(boolean ignoreEdge){
        this.ignoreEdge = ignoreEdge;
    }

    public void setReleaseListener(OnDragChangeLIstener listener){
        this.mDragListener = listener;
    }
}
